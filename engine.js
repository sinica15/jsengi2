function Game(WIDTH, HEIGHT, BCLR) {
    'use strict';

    let D = document;

    let cnv = D.createElement('canvas');
    cnv.setAttribute('width', WIDTH + 'px');
    cnv.setAttribute('height', HEIGHT + 'px');
    cnv.style.position = 'fixed';
    cnv.style.left = 0;
    cnv.style.top = 0;
    cnv.style.backgroundColor = BCLR;

    let createInput = function (name, type, id, left, top) {
        let div = document.createElement('div');
        // div.style.width = 100 + 'px';
        div.style.position = 'fixed';
        div.style.left = left + 'px';
        div.style.top = top + 'px';

        let label = document.createElement('label');
        label.setAttribute('for',id);
        label.innerHTML += name;


        let input = document.createElement('input');
        input.setAttribute('type', type);
        input.setAttribute('id', id);

        div.appendChild(input);
        div.appendChild(label);
        return div;
    };

    D.body.appendChild(cnv);
    D.body.appendChild(createInput('stop', 'checkbox', 'stop', 610, 0));
    D.body.appendChild(createInput('dev', 'checkbox', 'dev', 610, 20));

    let ctx = cnv.getContext('2d');

    let _Game = this;
    this.collisionDistance = 3;

    this.update = function () {
        console.log('Update Not Initialized');
    };

    let engine = function () {

        if (!document.getElementById('stop').checked){
            ctx.clearRect(0, 0, WIDTH, HEIGHT);
            _Game.overlap();
            _Game.update();
        }

        requestAnimationFrame(engine);
    };
    
    this.start = function () {
        engine();
    };

    this.overlap = function () {
        for (let i = 0; i < _Game.objects.length; i++){
            let obj = _Game.objects[i];

            for (let j = 0; j < _Game.objects.length; j++){
                if (j == i) continue;
                let objWith = _Game.objects[j];

                let distance = Math.sqrt(
                    Math.pow(obj.center[0] - objWith.center[0],2) +
                    Math.pow(obj.center[1] - objWith.center[1], 2)
                );

                // console.log(distance, obj.safeRadius, objWith.safeRadius);

                if(distance < obj.safeRadius + objWith.safeRadius){

                    if (
                        obj.angles[1][1] < objWith.angles[2][1] &&
                        obj.angles[3][1] > objWith.angles[0][1] &&
                        Math.abs(obj.angles[1][0] - objWith.angles[0][0]) < _Game.collisionDistance
                        ||
                        obj.angles[0][1] < objWith.angles[3][1] &&
                        obj.angles[2][1] > objWith.angles[1][1] &&
                        Math.abs(obj.angles[0][0] - objWith.angles[1][0]) < _Game.collisionDistance
                    ){
                        obj.vx *= -1;
                    }
                    if (
                        obj.angles[2][0] < objWith.angles[1][0] &&
                        obj.angles[3][0] > objWith.angles[0][0] &&
                        Math.abs(obj.angles[2][1] - objWith.angles[0][1]) < _Game.collisionDistance
                        ||
                        obj.angles[0][0] < objWith.angles[3][0] &&
                        obj.angles[1][0] > objWith.angles[2][0] &&
                        Math.abs(obj.angles[0][1] - objWith.angles[2][1]) < _Game.collisionDistance
                    ){
                        obj.vy *= -1;
                    }

                    continue;
                }
            }

            if (obj.static){
                continue;
            } else {
                // console.log(obj);
                if (
                    obj.angles[0][0] < 0 ||
                    obj.angles[3][0] > WIDTH
                ){
                    obj.vx *= -1;
                }
                if (
                    obj.angles[0][1] < 0 ||
                    obj.angles[3][1] > HEIGHT
                ){
                    obj.vy *= -1;
                }
                // console.log(obj);
            }
        }
    };

    this.objects = [];

    this.Rect = function (params) {
        this.x = params.x;
        this.y = params.y;

        this.vx = 0;
        this.vy = 0;

        if(params.vx){
            this.vx = params.vx;
        }
        if(params.vy){
            this.vy = params.vy;
        }

        this.width = params.width;
        this.height = params.height;
        this.color = params.color;

        this.angles = this.angleSearch(this.x, this.y, this.width, this.height);
        this.center = this.centerSearch(this.x, this.y, this.width, this.height);
        this.safeRadius = this.safeRadiusSearch(this.x, this.y, this.center);
    };

    this.Rect.prototype = {

        draw : function () {
            ctx.fillStyle = this.color;
            if (!this.static){
                this.angles = this.angleSearch(this.x, this.y, this.width, this.height);
                this.center = this.centerSearch(this.x, this.y, this.width, this.height);
            }

            ctx.fillRect(this.x, this.y, this.width, this.height);

            if (document.getElementById('dev').checked) {
                this.drawAngles(this.angles);
                this.drawCenter(this.center);
                this.drawSafeRadius(this.center, this.safeRadius);
            }
        },

        angleSearch : function (x, y, width, height) {
            let outDotsArr = [];

            outDotsArr.push([x, y]);
            outDotsArr.push([x + width - 1, y]);
            outDotsArr.push([x,y + height - 1]);
            outDotsArr.push([x + width - 1, y + height - 1]);

            return outDotsArr;
        },

        drawAngles : function (contD) {
            ctx.fillStyle = '#ff0000';
            for (let i = 0; i < contD.length; i++){
                ctx.fillRect(contD[i][0], contD[i][1], 1, 1);
            }
        },

        centerSearch : function (x, y, width, height) {
            return [ x + width/2, y + height/2 ];
        },
        
        drawCenter : function (center) {
            ctx.fillStyle = '#ff0000';
            ctx.fillRect(center[0], center[1], 1, 1);
        },

        safeRadiusSearch : function (x, y, center) {
            return (Math.sqrt(
                Math.pow((x - center[0]), 2) + Math.pow((y - center[1]), 2)
            ) + 1 );
        },

        drawSafeRadius : function (center, safeRadius) {
            ctx.fillStyle = '#ff0000';
            ctx.beginPath();
            ctx.arc(center[0], center[1], safeRadius, 0,2 * Math.PI, false);
            ctx.stroke();
        },

        move : function() {
            this.x += this.vx;
            this.y += this.vy;
        },

        shift : function () {
            if (!this.static){
                this.move();
            }
            this.draw();
        }

    };
}