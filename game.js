let game = new Game(600, 400, '#a29bff');

game.objects.push(
    new game.Rect({
        x : 250,
        y : 100,
        height : 110,
        width : 30,
        color : '#ffd51f',
        static : true
    }),
    new game.Rect({
        x : 310,
        y : 110,
        vx : -3.5,
        vy : 2.5,
        height : 20,
        width : 50,
        color : '#46ff40'
    }),
    new game.Rect({
        x : 110,
        y : 10,
        vx : -2.5,
        vy : 4.5,
        height : 40,
        width : 30,
        color : '#ff91fe'
    })
);

for (let i = 0; i < game.objects.length; i++){
    game.objects[i].shift();
}

game.update = function () {

    for (let i = 0; i < game.objects.length; i++){
        game.objects[i].shift();
    }

};

game.start();